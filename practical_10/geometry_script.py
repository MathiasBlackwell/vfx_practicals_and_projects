import bpy                          # Blender Python Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

console("STARTS:")

console("Changing Group Input attributes using scripting")

# Catch any errors
try:
    console("Setup Parameters")
    console("Default name 'GeometryNodes' if you change Geometry Node name you need to change this parameter")
    nodes_name = 'GeometryNodes'

    console("Setup Group Input Parameters by joining them with Node")
    console("In this example a Transform Node has been added to Geometry Node see Geometry Nodes Tab in Blender File")
    translation = 'Translation'
    scale = 'Scale'
    animation_speed = 'AnimationSpeed'
    keyFrames = 'Keyframes'

    console("Global Context Object")
    object = bpy.context.object
    
    console("Get Geometry Nodes")
    nodes = bpy.context.object.modifiers[nodes_name]

    console("Get Group Inputs")
    inputs = nodes.node_group.inputs

    console("Get Group Input for the Named Group Input, in this example it is Translation")
    translation = inputs[translation].identifier
    scale = inputs[scale].identifier
    speed = inputs[animation_speed].identifier
    enable_keyframes = inputs[keyFrames].identifier


    translation_path = f'["{translation}"]' # Formatting is inportant as it sets the translation_# ID for the Keyframe animation 
    scale_path = f'["{scale}"]'


    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 0
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=0)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=0) 

    nodes[translation][0] = 4
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=11)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=11)

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 8
    nodes[scale][0] = 1.5
    nodes[scale][1] = 1.5
    nodes[scale][2] = 1.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=21)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=21) 

    nodes[translation][0] = -4
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=31)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=31) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 0
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=41)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=41) 

    nodes[translation][0] = 0
    nodes[translation][1] = 4
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=51)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=51) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 8
    nodes[scale][0] = 1.5
    nodes[scale][1] = 1.5
    nodes[scale][2] = 1.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=61)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=61) 

    nodes[translation][0] = 0
    nodes[translation][1] = -4
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=71)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=71) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 0
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=80)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=80) 

    console("Update the Context (Flush Changes)")
    object.data.update()


except Exception as e:
    print(e)
    console(e)   

# Catch any errors
try:
    console("Setup Parameters")
    console("Default name 'GeometryNodes' if you change Geometry Node name you need to change this parameter")

    console("Get Geometry Nodes")
    node = bpy.data.node_groups["GeometryNodes2"]

    console("Get Group Inputs")
    inputs = node.inputs

    console(f"Input {inputs[1]}")

    console("Setup Group Input Parameters by joining them with Node")
    console("In this example a Transform Node has been added to Geometry Node see Geometry Nodes Tab in Blender File")
    translation = 'Translation'
    scale = 'Scale'
    animation_speed = 'AnimationSpeed'
    keyFrames = 'Keyframes'

    console("Global Context Object")
    object = bpy.context.object
    
    console("Get Geometry Nodes")
    nodes = bpy.context.object.modifiers[nodes_name]

    console("Get Group Inputs")
    inputs = nodes.node_group.inputs

    console("Get Group Input for the Named Group Input, in this example it is Translation")
    translation = inputs[translation].identifier
    scale = inputs[scale].identifier
    speed = inputs[animation_speed].identifier
    enable_keyframes = inputs[keyFrames].identifier


    translation_path = f'["{translation}"]' # Formatting is inportant as it sets the translation_# ID for the Keyframe animation 
    scale_path = f'["{scale}"]'


    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 8
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=0)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=0) 

    nodes[translation][0] = 0
    nodes[translation][1] = 4
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=11)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=11)

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 0
    nodes[scale][0] = 1.5
    nodes[scale][1] = 1.5
    nodes[scale][2] = 1.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=21)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=21) 

    nodes[translation][0] = 0
    nodes[translation][1] = -4
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=31)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=31) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 8
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=41)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=41) 

    nodes[translation][0] = 4
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=51)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=51) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 0
    nodes[scale][0] = 1.5
    nodes[scale][1] = 1.5
    nodes[scale][2] = 1.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=61)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=61) 

    nodes[translation][0] = -4
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=71)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=71) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 8
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=80)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=80) 

    console("Update the Context (Flush Changes)")
    object.data.update()


except Exception as e:
    print(e)
    console(e) 


# Catch any errors
try:
    console("Setup Parameters")
    console("Default name 'GeometryNodes' if you change Geometry Node name you need to change this parameter")

    console("Get Geometry Nodes")
    node = bpy.data.node_groups["GeometryNodes3"]

    console("Get Group Inputs")
    inputs = node.inputs

    console(f"Input {inputs[2]}")

    console("Setup Group Input Parameters by joining them with Node")
    console("In this example a Transform Node has been added to Geometry Node see Geometry Nodes Tab in Blender File")
    translation = 'Translation'
    scale = 'Scale'
    animation_speed = 'AnimationSpeed'
    keyFrames = 'Keyframes'

    console("Global Context Object")
    object = bpy.context.object
    
    console("Get Geometry Nodes")
    nodes = bpy.context.object.modifiers[nodes_name]

    console("Get Group Inputs")
    inputs = nodes.node_group.inputs

    console("Get Group Input for the Named Group Input, in this example it is Translation")
    translation = inputs[translation].identifier
    scale = inputs[scale].identifier
    speed = inputs[animation_speed].identifier
    enable_keyframes = inputs[keyFrames].identifier


    translation_path = f'["{translation}"]' # Formatting is inportant as it sets the translation_# ID for the Keyframe animation 
    scale_path = f'["{scale}"]'

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=0)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=0) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 6
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=11)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=11)

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 1.5
    nodes[scale][1] = 1.5
    nodes[scale][2] = 1.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=21)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=21) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 2
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=31)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=31) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=41)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=41) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 6
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=51)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=51) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 1.5
    nodes[scale][1] = 1.5
    nodes[scale][2] = 1.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=61)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=61) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 2
    nodes[scale][0] = 1
    nodes[scale][1] = 1
    nodes[scale][2] = 1
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=71)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=71) 

    nodes[translation][0] = 0
    nodes[translation][1] = 0
    nodes[translation][2] = 4
    nodes[scale][0] = 0.5
    nodes[scale][1] = 0.5
    nodes[scale][2] = 0.5
    nodes[speed] = 0.05
    nodes.keyframe_insert(data_path=translation_path, frame=80)  # Insert a Keyframe
    nodes.keyframe_insert(data_path=scale_path, frame=80) 


    console("Update the Context (Flush Changes)")
    object.data.update()

except Exception as e:
    print(e)
    console(e) 
console("ENDS:")
