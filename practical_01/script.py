import bpy

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False)

#cylinders
x = -6
y = 5
z = 1

for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_cylinder_add(location=(x,y,z))

#cones
x = -6
y = 5
z = 3

for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_cone_add(location=(x,y,z))

x = -6
y = 5
z = 5.5

for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_cone_add(location=(x,y,z))

#monkey head
x = -6
y = 5
z = 4 
for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_monkey_add(location=(x,y,z))


#torus'
x = -6
y = 5
z = 4.7
for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_torus_add(location=(x,y,z))
    
x = -6
y = 5
z = 2
for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_torus_add(location=(x,y,z))
    
x = -6
y = 5
z = 0
for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_torus_add(location=(x,y,z))


#sphere
x = -6
y = 5
z = 5

for x in range(-6,7,3):
    if (x <=0):
        y = y-2
    else:
        y = y+2
    bpy.ops.mesh.primitive_uv_sphere_add(location=(x,y,z))
    

