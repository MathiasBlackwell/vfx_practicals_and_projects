#Imports
import bpy, csv, os


bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False)


#Variables
#locations_file will be at home page, not folder, for GitLab I put a copy in the folder, but actual one was elsewhere
#I do not know how to fix that, but if it works, it works.
#This also if no file, will create on, find it. change values
locations_file = "locations.csv"

#
#
# Step 1: CREATE: A list
#
#
locations =[(1,1,1),
            (3,1,1),
            (5,1,1)]
cubeAmount = len(locations)

#
#
# Step 2: UPDATE: Generate them using blender
#
#
for count in range(cubeAmount):
    
    #Grabs numbers from cubes coords
    coords = locations[count] 
    
    #Draw the cubes
    bpy.ops.mesh.primitive_cube_add(location=(coords))
    #Count up, 
    count += 1
    
#
#
# Step 3 & 4: Add n amount of cubes, and update locations (All in one to see it)
#
#
locations =[(1,3,1),
          (3,3,1),
          (5,3,1),
          (7,3,1),
          (9,3,1)]
cubeAmount = len(locations)

#Redraw the scene like in step 2
for count in range(cubeAmount):
    
    #Grabs numbers from cubes coords
    coords = locations[count] 
    
    #Draw the cubes
    bpy.ops.mesh.primitive_cube_add(location=(coords))
    #Count up, 
    count += 1
    
#
#
# Step 5: Remove locations
#
#
locations =[(1,5,1),
            (3,5,1)]
cubeAmount = len(locations)

#Redraw the scene like in step 2
for count in range(cubeAmount):
    
    #Grabs numbers from cubes coords
    coords = locations[count] 
    
    #Draw the cubes
    bpy.ops.mesh.primitive_cube_add(location=(coords))
    #Count up, 
    count += 1
#
# 
# Step 6-9: Open file, read it, update scene
#
#
with open(locations_file) as csvfile:  
    content = csv.reader(csvfile, delimiter='\t', dialect='excel') # the .csv is seperated by tabs
    for row in enumerate(content):
        #turn the list, into a string, splitting it then where theres commas, turning it back into a tuple
        row = row[1]
        row = row[0].split(',')
        row = tuple(row)
        
        #empty list, take each string in row now, and append it. 
        final = []
        for i in row:
            i = float(i)
            final.append(i)
        print(row) 
        #FInally draw all the cubes with coords from sheet
        bpy.ops.mesh.primitive_cube_add(location=(final))
        