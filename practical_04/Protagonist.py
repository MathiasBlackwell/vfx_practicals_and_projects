import bpy                          # Blender Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

# Animation States
STATE_IDLE = "idle"
STATE_POG = "pog"
STATE_NEUTRAL = "neutral"

# Possible Inputs
EVENT_IDLE = "event_idle"
EVENT_POG = "event_pog"
EVENT_NEUTRAL = "event_neutral"

class Protagonist:
    def __init__(self):
        self.state = STATE_IDLE # initial state

    # Get current Protagonist State
    def getState(self):
        return self.state

    # Switch to new state based on event input
    def action(self, event):

        # Idle Transitions
        if self.state == STATE_IDLE:
            if event == EVENT_POG:
                console(f"State Transition : {self.state} => {STATE_POG}")
                self.state = STATE_POG
            elif event == EVENT_NEUTRAL:
                console(f"State Transition : {self.state} => {STATE_NEUTRAL}")
                self.state = STATE_NEUTRAL
            else:
                console(f"State unchanged : {self.state}")

        # Mouth POG Transitions
        elif self.state == STATE_POG:
            if event == EVENT_NEUTRAL:
                console(f"State Transition : {self.state} => {STATE_NEUTRAL}")
                self.state = STATE_NEUTRAL
            elif event == EVENT_IDLE:
                console(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                console(f"State unchanged : {self.state}")

        # Mouth NEUTRALd Transitions
        elif self.state == STATE_NEUTRAL:
            if event == EVENT_POG:
                console(f"State Transition : {self.state} => {STATE_POG}")
                self.state = STATE_POG
            elif event == EVENT_IDLE:
                console(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                console(f"State unchanged : {self.state}")