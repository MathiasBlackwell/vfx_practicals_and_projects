# VFX Practials and Projects

Project 1: IDAD10SecondAdvert

Status: Done

Link to Onedrive: https://setuo365-my.sharepoint.com/:v:/g/personal/c00261003_setu_ie/EbHQeAea7MZGlxLRJI7wEcgBAzlxj7QUgF2zx33HFdoPVA?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJPbmVEcml2ZUZvckJ1c2luZXNzIiwicmVmZXJyYWxBcHBQbGF0Zm9ybSI6IldlYiIsInJlZmVycmFsTW9kZSI6InZpZXciLCJyZWZlcnJhbFZpZXciOiJNeUZpbGVzTGlua0RpcmVjdCJ9fQ&e=LdebO2

Project 2: StartWithNothing

Status: Done

Link to Repository: https://bitbucket.org/vfx-project2/project2/downloads/

Link to Onedrive: https://setuo365-my.sharepoint.com/:v:/g/personal/c00261003_setu_ie/EXUuV9vS3_BJrU4GgjsCKwMBTre3wuI8BKUMVfq2BWRQ4A?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJPbmVEcml2ZUZvckJ1c2luZXNzIiwicmVmZXJyYWxBcHBQbGF0Zm9ybSI6IldlYiIsInJlZmVycmFsTW9kZSI6InZpZXciLCJyZWZlcnJhbFZpZXciOiJNeUZpbGVzTGlua0NvcHkifX0&e=PaNnHi

Practical 1: Primitive Shapes. 

Status: Done

Practical 2: CRUD

Status: Done

Practical 3: Classes

Status: Done

Practical 4: Animation

Status: Done

Practical 5: MakeMeshes

Status: Done

Practical 6: CameraTracking

Status: Done

Practical 7: Materials

Status: Done

Practical 8: Shaders

Status: No Submission

Practical 9: UVMapping

Status: No Submission

Practial 10: GeoNodes

Status: Done


