import random                       # Math Random
import bpy                          # Blender Python Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module
from math import pi, radians        # Transformations Rotation

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

console("STARTS:")

console("Defining a Plane Mesh")


vertices = [
        ( -0.0,   -0.0,   -1.5 ), # [0] Vertex 1
        ( -1.0,   +1.0,   -1.0 ), # [1] Vertex 2
        ( +2.0,   +2.0,   -1.0 ), # [2] Vertex 3
        ( +1.0,   -1.0,   -1.0 ), # [3] Vertex 4
        ( -3.0,   -3.0,   +1.0 ), # [4] Vertex 5
        ( -1.0,   +3.0,   +1.0 ), # [5] Vertex 6
        ( +1.0,   +1.0,   +1.0 ), # [6] Vertex 7
        ( +3.0,   -1.0,   +1.0 )  # [7] Vertex 8
]

# Define faces (index of vertices above)
faces = [
        (0, 1, 2, 3), # Front Face
        (7, 6, 5, 4), # Back Face
        (4, 5, 1, 0), # Left Face
        (7, 4, 0, 3), # Bottom Face
        (6, 7, 3, 2), # Right Face
        (5, 6, 2, 1)  # Top Face
]

edges =[]

x = 1
y = 1
z = 1


# Catch any errors
try:    
    console("Accessing Scenes")         
    result = bpy.data.scenes
    console(f"Scenes: {result}")

    for object in result:
        console(f"Scene Name: {object.collection.name}")

    console("Accessing Collections")
    result = bpy.data.collections
    console(f"Collections: {result}")

    for object in result:
        console(f"Collection Name: {object.name}")

    # Add custom collection, rename it and add it as a scene child
    collection = bpy.data.collections.new('Custom Collection')
    collection.name = "Scripted Objects"

    # Add the Collection to the Scene
    bpy.context.scene.collection.children.link(collection)

    # Set the new collection as the active one
    layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]
    bpy.context.view_layer.active_layer_collection = layer_collection

        
    for i in range(3):
       # Create mesh
        mesh_data = bpy.data.meshes.new("mesh_from_data")
        mesh_data.from_pydata(vertices, edges, faces)
        
        # Object using the mesh data
        object = bpy.data.objects.new("mesh_object", mesh_data)
        # Link the Object in the Scene
        bpy.context.collection.objects.link(object)


        # Add objects to Collection
        object.location =(x, y, z)


    console("Accessing Data-Blocks")
    console("Accessing Scene Collection")
    result = bpy.data.objects
    console(f"Objects in the Scene: {result}")

    console("Accessing Collections in a Scene")

    # Setup frame increment
    frames = 0
    frame_increment = 10
    offset = 10 # makes each object look a bit more dynamic

    for result in bpy.data.collections:
        if result.name == collection.name:
            for object in result.objects:
                
                # Starting Location
                frames = 0
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")
                
                # Transform Location
                frames += frame_increment
                object.location = (random.randint(1, 5), random.randint(1, 5), 1) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")     

                frames += frame_increment
                object.location = (random.randint(-10, 0), random.randint(-10, 0), 3) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-20, -10), random.randint(-20, -10), 5) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-30, -20), random.randint(-30, -20), 7) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-40, -30), random.randint(-40, -30), 5) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-50, -40), random.randint(-50, -40), 7) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-50, -40), random.randint(-50, -40), 9) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")



    # Save the blender file
    bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)

    console("ENDS:")

except Exception as e:
    console(e)
