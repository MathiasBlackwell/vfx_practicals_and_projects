import random                       # Math Random
import bpy                          # Blender Python Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module
from math import pi, radians        # Transformations Rotation

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

console("STARTS:")
console("Defining a Plane Mesh")
vertices = [
        ( -0.0,   -0.0,   -1.5 ), # [0] Vertex 1
        ( -1.0,   +1.0,   -1.0 ), # [1] Vertex 2
        ( +2.0,   +2.0,   -1.0 ), # [2] Vertex 3
        ( +1.0,   -1.0,   -1.0 ), # [3] Vertex 4
        ( -3.0,   -3.0,   +1.0 ), # [4] Vertex 5
        ( -1.0,   +3.0,   +1.0 ), # [5] Vertex 6
        ( +1.0,   +1.0,   +1.0 ), # [6] Vertex 7
        ( +3.0,   -1.0,   +1.0 )  # [7] Vertex 8
]
# Define faces (index of vertices above)
faces = [
        (0, 1, 2, 3), # Front Face
        (7, 6, 5, 4), # Back Face
        (4, 5, 1, 0), # Left Face
        (7, 4, 0, 3), # Bottom Face
        (6, 7, 3, 2), # Right Face
        (5, 6, 2, 1)  # Top Face
]
edges =[]
x = 1
y = 1
z = 1

# Catch any errors
try:    
    console("Accessing Scenes")         
    result = bpy.data.scenes
    console(f"Scenes: {result}")

    for object in result:
        console(f"Scene Name: {object.collection.name}")

    console("Accessing Collections")
    result = bpy.data.collections
    console(f"Collections: {result}")

    for object in result:
        console(f"Collection Name: {object.name}")

    # Add a camera with specified coordinates and rotation
    camera_location = (19.5027, 19.7113, 21.4734)
    camera_rotation = (radians(73.173), radians(1.54648), radians(134.335))
    bpy.ops.object.camera_add(enter_editmode=False, align='WORLD', location=camera_location, rotation=camera_rotation)
    camera = bpy.context.active_object
    camera.scale = (1, 1, 1)
    # Set the camera as the active camera in the scene
    bpy.context.scene.camera = camera


    # Add a light
    light_data = bpy.data.lights.new(name="Sun", type='SUN')
    light_object = bpy.data.objects.new(name="Sun", object_data=light_data)
    light_data.energy = 20 # 20 watts
    bpy.context.scene.collection.objects.link(light_object)
    #light_object.location = camera_object.location


    # Add custom collection, rename it and add it as a scene child
    collection = bpy.data.collections.new('Custom Collection')
    collection.name = "Planes"

    # Add the Collection to the Scene
    bpy.context.scene.collection.children.link(collection)

    # Set the new collection as the active one
    layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]
    bpy.context.view_layer.active_layer_collection = layer_collection  
        
    for p in range(5):
       # Create mesh
        mesh_data = bpy.data.meshes.new("mesh_from_data")
        mesh_data.from_pydata(vertices, edges, faces)
        
        # Object using the mesh data
        object = bpy.data.objects.new("mesh_object", mesh_data)
        # Link the Object in the Scene
        bpy.context.collection.objects.link(object)

        if p == 0:
            material = bpy.data.materials.new(name='Metallic_Blue')
            material.diffuse_color = (0.3, 0.7, 0.9, 1.0) # RGBA 'Blue'
            material.metallic = 1
            object.data.materials.append(material)
            object.modifiers.new('Scripted_Wireframe', 'WIREFRAME')
            object.modifiers['Scripted_Wireframe'].thickness = 0.5

        if p == 1:
            material = bpy.data.materials.new(name='Metallic_Green')
            material.diffuse_color = (0.3, 0.9, 0.3, 1.0) # RGBA 'Green'
            material.metallic = 1
            object.data.materials.append(material)
            object.modifiers.new('Scripted_Screw', 'SCREW')
            object.modifiers['Scripted_Screw'].iterations = 1

        if p == 2:
            material = bpy.data.materials.new(name='Metallic_Red')
            material.diffuse_color = (0.9, 0.2, 0.2, 1.0) # RGBA 'REd'
            material.metallic = 1
            object.data.materials.append(material)
            object.modifiers.new('Scripted_Bevel', 'BEVEL') # Subdivide Vertices
            object.modifiers['Scripted_Bevel'].width = 0.6

        if p == 3:
            material = bpy.data.materials.new(name='Metallic_Purple')
            material.diffuse_color = (0.9, 0.2, 0.9, 1.0) # RGBA 'Purple'
            material.metallic = 1
            object.data.materials.append(material)
            object.modifiers.new('Scripted_Bevel', 'BEVEL') # Subdivide Vertices
            object.modifiers['Scripted_Bevel'].width = 0.2

        if p == 4:
            material = bpy.data.materials.new(name='Metallic_White')
            material.diffuse_color = (0.9, 0.9, 0.9, 1.0) # RGBA 'White'
            material.metallic = 1
            object.data.materials.append(material)
            object.modifiers.new('Scripted_Mirror', 'MIRROR') # Subdivide Vertices


        # Add objects to Collection
        object.location =(x, y, z)

    console("Accessing Data-Blocks")
    console("Accessing Scene Collection")
    result = bpy.data.objects
    console(f"Objects in the Scene: {result}")

    console("Accessing Collections in a Scene")

    # Setup frame increment
    frames = 0
    frame_increment = 10
    offset = 10 # makes each object look a bit more dynamic

    for result in bpy.data.collections:
        if result.name == collection.name:
            for object in result.objects:
                
                # Starting Location
                frames = 0
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")
                
                # Transform Location
                frames += frame_increment
                object.location = (random.randint(1, 5), random.randint(1, 5), 1) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")     

                frames += frame_increment
                object.location = (random.randint(-15, 0), random.randint(-15, 0), 3) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-30, -15), random.randint(-30, -15), 5) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-45, -30), random.randint(-45, -30), 7) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-60, -45), random.randint(-60, -45), 5) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-75, -60), random.randint(-75, -60), 7) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                frames += frame_increment
                object.location = (random.randint(-90, -75), random.randint(-90, -75), 9) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")



    # Save the blender file
    bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)

    console("ENDS:")

except Exception as e:
    console(e)
